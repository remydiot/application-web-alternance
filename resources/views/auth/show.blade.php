@extends('layouts.app')

@section('content')
<!--

	Cette vue est le contenu de la page d'affichage d'un utilisateur.
	Elle propose egalement la modification de l'utilisateur et sa suppresion.

	La recherche fonctionne avec une route qui,lorsque je valide l'envoie de la demande me renvoie une requete comportant tous les utilisateurs demandés.
	J'utilise ensuite la directive de laravel "@)foreach('$details as $user')" pour lister les utilisateurs.

	La liste de tous les utilisateurs fonctionne de la même façon:
	J'utilise "@)foreach('App\User::all() as $user')" qui me permet de recuperer tous les utilisateurs et d'afficher les informations voulues (comme le nom de famille par exemple avec $user->lastname).
-->

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header text-center">

					<h1>

						{{$user->firstname}} {{$user->lastname}}

					</h1>

				</div>

				<div class="card-body">

					<table class="table">   <!-- Tableau qui affiche les informations de l'utilisateur choisi. -->

						<thead class=" text-center">

							<tr>
								<th>Attribut</th>
								<th>Valeur</th>
							</tr>

						</thead>

						<tbody>

							<tr>
								<td scope="row">Identificateur</td>

								<td>{{$user->id}}</td>

							</tr>

							<tr>

								<td scope="row">Nom</td>

								<td>{{$user->lastname}}</td>

							</tr>

							<tr>

								<td scope="row">Prénom</td>

								<td>{{$user->firstname}}</td>

							</tr>

							<tr>

								<td scope="row">Email</td>

								<td>{{$user->email}}</td>

							</tr>

							<tr>
								<td scope="row">Date de naissance</td>

								<td>{{$user->birthdate}}</td>

							</tr>

							<tr>

								<td class="text-center">

								@if ($user->id == auth()->user()->id)   <!-- Désactive l'option de suppression de l'utilisateur si l'on est connecté avec ce dernier.-->

									<p>Vous etes actuellement connecté avec cet utilisateur.</p>
									<p>Veuillez changer d'utilisateur avant de suprrimer celui ci.</p>

								@else

								<form action="/home/{{$user->id}}/delete" method="POST">

									@csrf
									@method('delete')

									<button type="submit" class="btn btn-danger">

										Delete

									</button>

								</form>

								@endif

								</td>
								<td class="text-center">

									<a name="modify"
									id="modify"
									class="btn btn-primary"
									href="/home/{{$user->id}}/edit"
									role="button">

										Modifier

									</a>

								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

