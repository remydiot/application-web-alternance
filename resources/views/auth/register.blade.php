@extends('layouts.app')

@section('head')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

@endsection


@section('content')
<!--

	Cette vue est le contenu de la page d'inscription fournie par le projet laravel.

	J'y ai fait des modifications pour pouvoir la réutiliser lorsque l'on est déjà connecté et eviter d'avoir l'autoauthetification apreès l'inscription.
	Pour y parvenir j'ai simplement decider d'envoyer la requete sur une route differente lorsque l'on est déhà connecté (à l'aide de la directive @)guest )

	Les importations de scripts et de feuille de style plus haut permettent la gestion du datepicker.
-->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<script src="bootstrap-datepicker.fr.js" charset="UTF-8"></script>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">

				@guest

					<div class="card-header">{{ __('Inscription') }}</div>

				@else

					<div class="card-header">{{ __('Inscription d\'un nouvel utilisateur.') }}</div>

				@endguest


				<div class="card-body">

				@guest

					<form method="POST" action="{{ route('register') }}">   <!-- Si vous n'êtes pas connectez, le formulaire renvera vers la route 'register' classique et vous connectera avec les identifiants fraîchement créer.-->

				@else

					<form method="POST" action="{{ route('home') }}">   <!-- Si vous êtes connectez, vous êtes redirigez vers la route 'home' (!!! La verification de mot de passe ne fonctionne pas avec cette methode !!!). -->

				@endguest

						@csrf

						<div class="form-group row">

							<label for="firstname" class="col-md-4 col-form-label text-md-right">

								{{ __('Prénom : ') }}

							</label>

							<div class="col-md-6">

								<input
								id="firstname"
								type="text"
								class="form-control @error('firstname') is-invalid @enderror capitalized"
								name="firstname"
								value="{{ old('firstname') }}"
								autocomplete="off"
								required
								autofocus
								>

								@error('firstname')

									<span class="invalid-feedback" role="alert">

										<strong>{{ $message }}</strong>

									</span>

								@enderror

							</div>

						</div>

						<div class="form-group row">

							<label for="lastname" class="col-md-4 col-form-label text-md-right">

								{{ __('Nom : ') }}

							</label>

							<div class="col-md-6">

								<input
								id="lastname"
								type="text"
								class="form-control @error('lastname') is-invalid @enderror capitalized"
								name="lastname"
								value="{{ old('lastname') }}"
								autocomplete="off"
								required
								>

								@error('lastname')

									<span class="invalid-feedback" role="alert">

										<strong>{{ $message }}</strong>

									</span>

								@enderror

							</div>

						</div>

						<div class="form-group row">

							<label for="email" class="col-md-4 col-form-label text-md-right">

								{{ __('Adresse Mail : ') }}

							</label>

							<div class="col-md-6">
								<input
								id="email"
								type="email"
								class="form-control @error('email') is-invalid @enderror verif-mail"
								name="email"
								value="{{ old('email') }}"
								autocomplete="off"
								required
								>

								@error('email')

									<span class="invalid-feedback" role="alert">

										<strong>{{ $message }}</strong>

									</span>

								@enderror
							</div>

						</div>

						<div class="form-group row">

							<label for="password" class="col-md-4 col-form-label text-md-right">

								{{ __('Mot de passe : ') }}

							</label>

							<div class="col-md-6">

								<input
								id="password"
								type="password"
								class="form-control @error('password') is-invalid @enderror"
								name="password"
								autocomplete="off"
								required
								>

								@error('password')

									<span class="invalid-feedback" role="alert">

										<strong>{{ $message }}</strong>

									</span>

								@enderror

							</div>

						</div>

						<div class="form-group row">

							<label for="password-confirm" class="col-md-4 col-form-label text-md-right">

								{{ __('Confirmez le mot de passe : ') }}

							</label>

							<div class="col-md-6">
								<input
								id="password-confirm"
								type="password"
								class="form-control"
								name="password_confirmation"
								autocomplete="off"
								required
								>
							</div>

						</div>

						<div class="form-group row">

							<label for="birthdate" class="col-md-4 col-form-label text-md-right">

								{{ __('Date de naissance : ') }}

							</label>

							<div class="col-md-6">
                                            <!-- Choix la date de naissance par un datepicker bootstrap.-->
								<input
								class="date form-control @error('birthdate') is-invalid @enderror"
								data-date-end-date="0d"
								type="text"
								name="birthdate"
								autocomplete="off"
								required
								>

								@error('birthdate')

									<span class="invalid-feedback" role="alert">

										<strong>{{ $message }}</strong>

									</span>

								@enderror

							</div>

						</div>

						<script type="text/javascript">

							$('.date').datepicker({

								weekStart: 1,
								format: 'yyyy-mm-dd',

							});

						</script>


						<div class="form-group row mb-0">

							<div class="col-md-6 offset-md-4">

								<button type="submit" class="btn btn-primary">

									{{ __('Inscription') }}

								</button>

							</div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" charset="utf-8">

	//Script permettant la capitalisation automatique.

	jQuery(document).ready(function($) {

		$('.capitalized').keyup(function(event) {

			var textBox = event.target;

			var start = textBox.selectionStart;

			var end = textBox.selectionEnd;

			textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);

			textBox.setSelectionRange(start, end);

		});

	});

</script>

@endsection

