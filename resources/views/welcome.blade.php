<!--

	Cette vue est le contenu de la page d'accueil de l'application.
	Elle affiche un message de bienvenue, vous invitant à vous connecter ou vous insrire si vous ne l'êtes pas et vous affiche votre nom et prénom si vous l'êtes.

-->


@extends('layouts.app')


@section('content')
<div class="container" ">
	<table style="height: 500px;"">
		<tr>
			<td class="align-middle">
				<h1 class="display-1">

				Bienvenue,<br>

				@guest

					veuillez vous connecter, ou vous inscrire.

				@else

					{{auth()->user()->firstname}} {{auth()->user()->lastname}}.

				@endguest

				</h1>
			</td>
		</tr>
	</table>
</div>



@endsection
