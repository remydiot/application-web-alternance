<!doctype html>
<!--

	Cette vue est la base de toutes les autres vue j'y utilise le CSS de bootstrap 4.5.0 ainsi que la feuille fontawesome pour l'icone de la recherche sur la vue 'home'.

-->


<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}" defer></script>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">

	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

	<!-- Styles -->

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	@yield('head')
</head>

<body>

	<div id="app">

		<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">

			<div class="container">

				<a class="navbar-brand" href="{{ url('/') }}">

					L'applicaion web de Rémy Carrette.

				</a>

				<button
				class="navbar-toggler"
				type="button"
				data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent"
				aria-expanded="false"
				aria-label="{{ __('Toggle navigation') }}"
				>

					<span class="navbar-toggler-icon"></span>

				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<!-- Left Side Of Navbar -->
					<ul class="navbar-nav mr-auto">

					</ul>

					<!-- Right Side Of Navbar -->
					<ul class="navbar-nav ml-auto">
						<!-- Authentication Links -->
						@guest  <!-- Affichage des liens de connexion ou de redirection vers l'accueil en fonction de l'état actuel (connecté ou non) de l'utilisateur.-->

							<li class="nav-item">

								<a class="nav-link" href="{{ route('login') }}">{{ __('Connexion') }}</a>

							</li>

							@if (Route::has('register'))

								<li class="nav-item">

									<a class="nav-link" href="{{ route('register') }}">{{ __('Inscription') }}</a>

								</li>

							@endif

						@else

							<li class="nav-item">

								<a class="nav-link" href="{{ route('home') }}">{{ __('Accueil') }}</a>

							</li>

							<li class="nav-item dropdown">
								<a
								id="navbarDropdown"
								class="nav-link dropdown-toggle"
								href="#" role="button"
								data-toggle="dropdown"
								aria-haspopup="true"
								aria-expanded="false"
								v-pre
								>
									{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
									<span class="caret"></span>

								</a>

								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

									<a class="dropdown-item" href="{{ route('logout') }}"

									   onclick="event.preventDefault();

													 document.getElementById('logout-form').submit();">

										{{ __('Deconnexion') }}

									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

										@csrf

									</form>
								</div>
							</li>
						@endguest
					</ul>
				</div>
			</div>
		</nav>

		<main class="py-4">

			@yield('content')

		</main>

	</div>
</body>
</html>
