<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = RouteServiceProvider::HOME;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		//$this->middleware('guest');							//Mettre en commentaire cette ligne permet d'acceder à la vue 'register' en étant connecté.
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'firstname' => ['required', 'string', 'max:255'],						//Ici je verifie le prénom.
			'lastname' => ['required', 'string', 'max:255'],                        //Ici je verifie le nom.
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			'password' => ['required', 'string', 'min:8', 'confirmed'],
			'birthdate' => ['required','string']                                    //J'ajoute la verification de la date de naissance.
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\User
	 */
	protected function create(array $data)
	{
		return User::create([
			'firstname' => $data['firstname'],                                      //Création du prénom de l'utilisateur
			'lastname' => $data['lastname'],                                        //Création du nom de l'utilisateur
			'email' => $data['email'],
			'password' => Hash::make($data['password']),
			'birthdate' => $data['birthdate']                                       //Création de la date de naissance de l'utilisateur.
		]);
	}
}
