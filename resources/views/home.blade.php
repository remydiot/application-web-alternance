


@extends('layouts.app')

@section('content')
<!--

	Cette vue est le contenu de la page d'affichage des utilisateurs.
	Elle propose egalement la recherche d'utilisateurs à l'aide de la barre de recherche.

	La recherche fonctionne avec une route qui,lorsque je valide l'envoie de la demande me renvoie une requete comportant tous les utilisateurs demandés.
	J'utilise ensuite la directive de laravel "@)foreach('$details as $user')" pour lister les utilisateurs comme je le souhaite.

	LA liste de tous les utilisateurs fonctionne de manière analogue:
	J'utilise "@)foreach('App\User::all() as $user')" qui me permet de recuperer tous les utilisateurs et d'afficher les informations que désire(comme le nom de famille par exemple avec $user->lastname).
-->

<div class="container">
	<div class="row justify-content-center">
		<div class="col">
			<div class="card">
			<div class="card-header">Bienvenue {{Auth::user()->firstname}}.</div>
				<div class="card-body">

					@if (session('status'))

						<div class="alert alert-success" role="alert">

							{{ session('status') }}

						</div>

					@endif

					<form action="/search" method="POST" role="search"> <!-- Formulaire qui va renvoyer les informations de recherche.-->

						@csrf

						<div class="input-group">

							<input
							type="text"
							class="form-control"
							name="q"
							placeholder="Cherchez le prénom, nom, email ou date de naissance(yyyy-mm-dd) d'un utilisateur."
							autocomplete="off"
							required
							>

							<span class="input-group-btn">

								<button type="submit" class="btn btn-default">

									<span class="fas fa-search"></span>

								</button>
							</span>
						</div>
					</form>

					<div class="container">

					@if(isset($details))    <!-- Message qui diffère selon que l'on aie ou pas d'utilisateur correspondant. -->

						<h2 class="display-5">

							Voici les utilisateurs qui correspondent à <b> {{ $query }} </b> :

						</h2>

						<table class="table table-striped"> <!-- Tableau qui liste les utilisateurs trouvés.-->

							<thead>

								<tr>

									<th scope="col">id</th>
									<th scope="col">Prénom</th>
									<th scope="col">Nom</th>
									<th scope="col">Adresse Mail</th>
									<th scope="col">Date de Naissance</th>
								</tr>

							</thead>

							<tbody>

							@foreach($details as $user)

								<tr>

									<th scope="row">{{$user->id}}</th>

										<td>

											<a href="/home/{{$user->id}}">

												{{$user->firstname}}

											</a>

										</td>

										<td>{{$user->lastname}}</td>

										<td>{{$user->email}}</td>

										<td>{{$user->birthdate}}</td>

									</tr>

							@endforeach

							</tbody>

						</table>

						<hr>

					@elseif(isset($message))    <!-- Message d'erreur si l'on a aucun utilisateur qui corresponds aux champs envoyés.-->

						<p class="alert alert-danger">{{ $message }}</p>

					@endif

					</div>

					<h2>Voici tous les utilisateurs du site:</h2>

					<p>Cliquez sur leur prénom pour avoir le détail.</p>

					<a href="/register" class="btn btn-primary "> Ajouter un utilisateur</a>	<!-- Bouton qui permet de renvoyer vers la page de crétion d'n nouvel utilisateur. -->

					<table class="table table-striped ">	<!--Tableau qui recense tous les utilisateurs déjà enregistrés. -->

						<thead>

							<tr>

								<th scope="col">id</th>
								<th scope="col">Prénom</th>
								<th scope="col">Nom</th>
								<th scope="col">Adresse Mail</th>
								<th scope="col">Date de Naissance</th>

							</tr>

						</thead>

						<tbody>

							@foreach(App\User::all() as $user)

						<tr>

							<th scope="row">

								{{$user->id}}

							</th>
							<td>
								<a href="/home/{{$user->id}}">

									{{$user->firstname}}

								</a>
							</td>
								<td>{{$user->lastname}}</td>

								<td>{{$user->email}}</td>

								<td>{{$user->birthdate}}</td>

						</tr>

						@endforeach

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
