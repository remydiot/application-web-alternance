# À propos de cette application.

## Comment j'ai construit le projet.

- J'ai installé le projet laravel par défaut via composer et ajouté le package laravel/ui pour fournir les composants de base du système d'authentification.

## Comment l'utiliser.

- Sur l'écran de présentation, vous pouvez choisir de vous connecter ou de vous enregistrer.

- Si vous souhaitez vous connecter en cliquant sur le bouton "Connexion", vous serez redirigé vers la vue 'login'. Vous aurez besoin d'un e-mail et d'un mot de passe déjà existants;vous serez redirigé vers la vue 'home'.

- Si vous souhaitez vous enregistrer en cliquant sur le bouton "Inscription", vous serez redirigé vers la vue 'register', vous serez automatiquement connecté et redirigé vers la vue 'home'. Les champs prénom et nom sont automatiquement mis en majuscule sur la première lettre, l'e-mail est également vérifié.


- Sur la vue 'home', vous avez tout d'abord un message d'accueil et juste en dessous une barre de recherche qui offre des possibilités de recherche. Vous pouvez utiliser le prénom ou le nom ou la date de naissance pour trouver l'utilisateur.

- La liste des résultats apparaîtra juste sous la barre de recherche à l'intérieur d'un tableau. Vous pourrez toujours voir la fin du contenu de la vue 'accueil'.

Ensuite, vous avez tous les utilisateurs enregistrés dans un tableau affichant les informations des utilisateurs et un bouton "Ajouter un utilisateur".

- Vous pouvez cliquer sur le prénom des utilisateurs pour accéder à la vue 'show'.

- Vous pouvez cliquer sur le bouton "Ajouter un utilisateur" pour accéder à la vue 'register', c'est la même vue que pour l'inscription mais avec un message d'accueil différent et un comportement différent:
Une fois que vous êtes connecté et que vous avez rempli le formulaire, vous serez redirigé vers la vue 'home'.

- Dans la vue 'show' de l'utilisateur, vous pouvez voir toutes les infos (à l'exception du mot de passe) et il y a deux boutons: "Supprimer" et "Modifier".

- Le bouton "Supprimer" supprimera l'utilisateur de la base de données et vous redirigera vers la vue 'home' s'il n'est pas l'utilisateur avec lequel vous êtes actuellement connecté.

- Le bouton "Modifier" vous redirigera vers la vue 'edit'.


- Dans la vue d'édition, vous avez un formulaire avec des informations de saisie déjà remplies, vous pouvez le modifier et soumettre les modifications en cliquant sur le bouton "Modifier".


- Les autres vues 'verify.blade.php', 'passwords/confirm.blade.php', 'passwords/email.blade.php', 'passwords/reset.blade.php' sont des vues par défaut fournies avec laravel/installation de l'interface utilisateur. Je ne les ai pas touchés du tout.

## Résumé de mon travail

### Bases de la base de données.

- La base de données se trouve dans le dossier "BBD", et est nommé "exerciceDB", elle peut être utilisée dans mysql.



### Côté backend.

- Le système d'enregistrement accepte désormais 'prénom', 'nom de famille', 'e-mail', 'mot de passe' et 'date de naissance' via un datepicker bootstrap.

- J'ai modifié la migration utilisateur, le registerController et le modèle utilisateur pour l'adapter à la nouvelle définition de l'utilisateur.

- J'ai ajouté toutes les nouvelles routes dans le fichier 'routes/web.php' car j'utilise la classe Auth\User par défaut de laravel/ui et je ne voulais pas jouer avec un autre modèle. J'ai essayé de m'en tenir au modèle Eloquent (Index, Show, Create, Store, Edit, Update, Delete) et j'ai ajouter une route Search pour la recherche d'utilisateurs.


### Côté frontend.

- J'ai codé en dur la traduction des vues en français.

- La vue 'home' affiche désormais la liste des utilisateurs enregistrés, un bouton permet d'enregistrer un nouvel utilisateur en utilisant la route 'register'.

- La vue 'show' affiche un tableau avec toutes les infos sur un utilisateur deux boutons sont là: un pour supprimer l'utilisateur, un pour modifier la base de données (/!\ Pas l'identifiant et le mot de passe /!\).


### Ce qui pourrait être amélioré

- Le format de la date est aaaa-mm-jj.

- Vous ne pouvez pas spécifier le prénom et le nom et la date de naissance dans la barre de recherche; un seul des trois par requête.

- Vous ne pouvez pas modifier le mot de passe d'un utilisateur existant.

- L'itinéraire de suppression pourrait être amélioré avec un message de confirmation.


_______________________________________________________________________________________________________________________________________________
_______________________________________________________________________________________________________________________________________________
_______________________________________________________________________________________________________________________________________________


# About this application.

## How I built the Project .

-	I installed the default laravel project thru composer and added the package laravel/ui to provide basic components of the authentication system.

## How to use it.

-	On the front screen you can choose to connect or to register yourself.

	-	If you want to connect yourself by clicking on the "Connexion" button, you will be redirected to the 'login' view. You will need an already existing email and password credentials and be redirected to the 'home' view.

	-	If you want to register yourself by clicking on the "Inscription" button, you will be redirected to the 'register' view, you will be automatically sign in and be redirected to the 'home' view. The firstname and lastname field are automaticly capitalized the email is verified too.


-	On the 'home' view you have firstly a greeting message and right after a search bar that provide research possibilities. You can use firstname or lastname or birthdate to find the user.

	-	The result list will appear right under the research bar inside a table. You will still be able to see the end of the 'home' view content.

	Next you have all the registrated users in a table  showing users infos and an "Ajouter un utilisateur" button.

	-	You can click on the users firstname to acces the 'show' view.

	-	You can click on the "Ajouter un utilisateur"  button to acces the 'register' view, it is the same view but with a different greeting message and a different behaviour:
		Once you are logged in and you filled the form you will be redirected to the 'home' route not the 'register'.

-	On the 'show" view of the user you can see all the infos (except for the password) and there is two buttons: "Supprimer" and "Modifier".

	-	The "Supprimer" button will delete the user from the database and redirect you to the 'home' view if it is not the user you are currently connected with.

	-	The "Modifier" button will redirect you to the 'edit' view.


-	On the edit view you have a form with already filled input informations, you can change it and submit the changes by clicking the "Modifier" button.


-	The other views 'verify.blade.php', 'passwords/confirm.blade.php', 'passwords/email.blade.php', 'passwords/reset.blade.php' are default views that came with the laravel/ui installation. I did not touch them at all.

## Summary of my work

### Database basics.

-	The database is in the folder "BBD", named "exerciceDB" and can be used in mysql.



### Backend side.

-	The register system now accept 'firstname', 'lastname', 'email', 'password' and 'birthdate' thru a bootstrap datepicker.

-	I modified the User migration, the registerController and the User Model to fit with the new definition of the user.

-	I added all the new routes in the "routes/web.php" file because I am using the default Auth\User::class of laravel/ui and did not wanted to mess around with another Model. I tried to stick with the Eloquent partern (Index, show, create, store, edit, update, delete) and add a search route.


### Frontend side.

-	I hardcoded the translation of the views in french but still write in english everywhere esle.

-	The 'home' view now display the list of the registrated users, a button permite to register a new user by using the 'register' route.

-	The 'show' view display a table with all the infos about one user two buttons are there: one to delete the user, one to modify the database (/!\ not the id and the password /!\)


### What could be improved

-	The date format is yyyy-mm-dd.

-	You cannot specify firstname and lastname and birthdate in the search bar; just one of the three per query.

-	You cannot modify the password of an existing user.

-	The delete route could be improved with a soft delete and a confirm message.
