<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//WELCOME route.
Route::get('/', function () {
	return view('welcome');
});

//AUTH routes.
Auth::routes();


//EDIT route. Renvoie à la view auth.edit et renvoie un objet User pour pouvoir remplir automatiquement les champs d'input.
Route::get('/home/{user}/edit', function (User $user) {

	return view('auth.edit', ['user'=>$user]);
});


//SHOW route. Renvoie à la view auth.edit et renvoie un objet User pour pouvoir remplir automatiquement le tableau d'information.
Route::get('/home/{user}', function (User $user) {

	return view('auth.show', ['user'=>$user]);
});


//UPDATE route. Permet de récuperer les informations passées par la route EDIT et de modifier la base de donnée en verifiant les informations nouvellement données. Et renvoie à la view 'home'.
Route::put('/home/{user}', function(User $user){

	$user->update(request()->validate([
		'firstname' => ['required', 'string', 'max:255'],
		'lastname' => ['required', 'string', 'max:255'],
		'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
		'birthdate' => ['required', 'string', 'max:10'],

	]));

	return redirect(route('home'));
});

// RESEARCH route. Récupère le champ rempli par l'utilisateur et cherche dans
// la base de donnée si il corespond à un prénom, un nom, une adresse mail ou une date de naissance.
//
// Il renvoie ensuite à la pview 'home' avec les utilisateurs trouvé ou avec un message d'erreur qui
// sera affiché au besoin sous la barre de recherche.

Route::any('/search',function(){

	$q = request()->get( 'q' );

	$user = User::where('firstname','LIKE','%'.$q.'%')
	->orWhere('lastname','LIKE','%'.$q.'%')
	->orWhere('email','LIKE','%'.$q.'%')
	->orWhere('birthdate','LIKE','%'.$q.'%')->get();

	if(count($user) > 0)

		return view('home')->withDetails($user)->withQuery ( $q );

	else

		return view ('home')->withMessage('Aucun utilisateur ne corresponds à '.$q.'.');
});

// CREATE route. Route qui récupère les champs de textes remplis lors de l'inscription par un utilisateur déjà connecté.
// Procède à la verification des champs
Route::post('/home', function() {

	request()->validate([
		'firstname' => 'required',
		'lastname' => 'required',
		'email' => 'required',
		'password' => ['required', 'string', 'min:8', 'confirmed'],
		'birthdate' => 'required'

	]);
	$user = new User(request(['firstname','lastname','email','password','birthdate']));
	$user->save();

	return redirect(route('home'));

});

//HOME route.
Route::get('/home', 'HomeController@index')->name('home');

//DELETE route.
Route::delete('/home/{user}/delete', function($id){

	$user = App\User::find($id);
	$user->delete();
	return view('home');
});

