@extends('layouts.app')

@section('head')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

@endsection


@section('content')
<!--

	Cette vue est le contenu de la page d'édition des informtations d'un utilisateur.

	Je récupère les informations de la base de données que je stocke dans les champs d'input, puis je redirige le fomrulaire remplis grâce à une route d'upload.

-->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<script src="bootstrap-datepicker.fr.js" charset="UTF-8"></script>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">

				<div class="card-header text-center">

					<h1>

						{{$user->firstname}} {{$user->lastname}}

					</h1>

				</div>
				<div class="card-body">

					<form action="/home/{{$user->id}}"  method="POST" role="form">  <!-- Formulaire qui redirige vers la page de detail de l'utilisateur qui a été modifié et mis à jour -->

						@csrf
						@method('PUT')  <!--Directive laravel qui permet de dire "je veux la methode PUT même si le navigateur ne le comprends pas.-->

						<div class="form-group capitalized">

							<label for="lastname">Nom</label>
							<input
							type="text"
							name="lastname"
							id="lastname"
							class="form-control"
							value="{{ $user->lastname }}"
							required
							>

						</div>

						<div class="form-group capitalized">

							<label for="firstname">Prénom</label>
							<input
							type="text"
							name="firstname"
							id="lastname"
							class="form-control"
							value="{{ $user->firstname }}"
							required
							>

						</div>

						<div class="form-group">

						  <label for="email">

							  Adresse Mail

							</label>

							<input
							type="email"
							class="form-control"
							name="email"
							id="email"
							value="{{ $user->email }}"
							required
							>

						</div>

						<div class="form-group">

							<label for="birthdate">

								Date de naissance

							</label>

							<input
							class="date form-control"
							data-date-end-date="0d"
							type="text"
							name="birthdate"
							value="{{ $user->birthdate }}"
							required
							>
						</div>

						<script type="text/javascript">

							$('.date').datepicker({
								weekStart: 1,
								format: 'yyyy-mm-dd',

							 });

						</script>

						<div class="form-group align-center">

							<button type="submit" class="btn btn-primary ">

								Modifier

							</button>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" charset="utf-8">

    //Script permettant la capitalisation automatique.

	jQuery(document).ready(function($) {

		$('.capitalized').keyup(function(event) {

			var textBox = event.target;

			var start = textBox.selectionStart;

			var end = textBox.selectionEnd;

			textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);

			textBox.setSelectionRange(start, end);

		});

	});

</script>

@endsection
