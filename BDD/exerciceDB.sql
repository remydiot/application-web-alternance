-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: exerciceDB
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Remy','Carrette','carretteremy@outlook.fr',NULL,'$2y$10$bKYWG.xNBtzNkBarHWDFSuy.NpdToaPTQIggvCQwsaQvtBjnwSBZK','1994-04-30',NULL,'2020-06-04 17:45:44','2020-06-04 17:45:44'),(2,'Americo','Herman','steve88@example.net','2020-06-04 17:46:01','$2y$10$5mebAnK7JWWnrvk94a/sb.h4DqFgwyIP.A2KCe5qQHWfcD67aB6vS','2002-01-10','2q0PsiiuHK','2020-06-04 17:46:02','2020-06-04 17:46:02'),(3,'Camron','Bechtelar','kamron00@example.net','2020-06-04 17:46:01','$2y$10$aB.pFa.nbnp9J7MYEgyMEOFQtS/Pkmw1S6q7VQVCAJ4xd6suq5jLO','2020-04-19','RvAZEETN3n','2020-06-04 17:46:02','2020-06-04 17:46:02'),(4,'Modesta','Schmitt','tarmstrong@example.com','2020-06-04 17:46:02','$2y$10$dL54nUPaXb0uX37k5vu61O9l6WtDk9HCWydFLn3SS20wu8PyR5O.y','1975-05-25','Iu1YAJyVE9','2020-06-04 17:46:02','2020-06-04 17:46:02'),(5,'Iliana','Bednar','carroll.tremaine@example.net','2020-06-04 17:46:02','$2y$10$EIbZ23DFQtywPCvF3kyewOmkE93TfMWg0.rY4Nsosi8NNp9s48/7e','1985-09-14','HtsZLq6qw0','2020-06-04 17:46:02','2020-06-04 17:46:02'),(6,'Kristy','Rowe','verda.parisian@example.net','2020-06-04 17:46:02','$2y$10$Y10BMQGazioNCvFo8EKUAuzgLcq/LNUsCrjSNFbfkBQuuT7sP.FSu','2013-10-27','ge3i1V6SbM','2020-06-04 17:46:02','2020-06-04 17:46:02'),(7,'Trinity','Price','rod.ruecker@example.com','2020-06-04 17:46:02','$2y$10$ekCJeYTV/UXKRt3axcFuvel2tq0d3fkqhyJGT0eqN7LJwLmi2hcDm','2011-04-22','nl5gJBd6SY','2020-06-04 17:46:02','2020-06-04 17:46:02'),(8,'Anderson','Carroll','erick63@example.com','2020-06-04 17:46:02','$2y$10$2YnsnRCVUTcYx6rTx.nz4egPeqLqKemAJy6b12gkloADwZUL3zFpG','1993-11-29','jyQy5uYQ4A','2020-06-04 17:46:02','2020-06-04 17:46:02'),(9,'Payton','Wolff','claudie.will@example.org','2020-06-04 17:46:02','$2y$10$XbQlr318jR9qtEqIU0AQzOdvOpYloCtPFV0Jx1B9rE16NKBZK7bBm','2016-03-20','sqemiKltzO','2020-06-04 17:46:02','2020-06-04 17:46:02'),(10,'Oscar','Lockman','allan85@example.net','2020-06-04 17:46:02','$2y$10$JE/PPYoxXrt.goYDMMzumOGBYoclF0VSlHqvlhqYXyXKv9eQvjvHq','2018-10-03','w1xs8EyHLN','2020-06-04 17:46:02','2020-06-04 17:46:02'),(11,'Cathy','Muller','london34@example.org','2020-06-04 17:46:02','$2y$10$lYPKYPFVD5LQ4O03EEC3Ze.jNenKOjDRsoboCBR0EX.MEseCzfMdy','1990-11-28','TRiFy0GXom','2020-06-04 17:46:02','2020-06-04 17:46:02');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-04 21:50:43
