<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
	return [
		'firstname' => $faker->firstname,       //Mise en place des prénoms de la factory d'utilisateurs.
		'lastname' => $faker->lastname,         //Mise en place des noms de la factory d'utilisateurs.
		'email' => $faker->unique()->safeEmail,
		'email_verified_at' => now(),
		'password' => Hash::make('secure1234'), // le mot de passe est secure1234.
		'birthdate' => $faker->date,            //Mise en place de la date de naissance pour la factory d'utilisateurs.
		'remember_token' => Str::random(10),
	];
});
